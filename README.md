ARTIPHONIE - Interface Administrateur
================================================================

*Vous trouverez dans ce répertoire tout le contenu lié à l'interface d'administration web, développé en VueJS et à destination des orthophonistes.*  

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).