import { createApp } from 'vue'
import { createStore } from 'vuex'
import createPersistedState from "vuex-persistedstate";
import App from './App.vue'
import PrimeVue from 'primevue/config'
import Tooltip from 'primevue/tooltip'
import router from './router'
import axios from 'axios'

import 'primevue/resources/themes/saga-orange/theme.css'    //theme
import 'primevue/resources/primevue.min.css'                //core css
import 'primeicons/primeicons.css'                          //icons
import '/node_modules/primeflex/primeflex.css'              //prime flex

axios.defaults.withCredentials = true;
axios.defaults.baseURL = 'http://localhost:5000/';
//axios.defaults.baseURL = 'https://artiphonieback.rakura.fr/';  // the FastAPI backend

const store = createStore({
    state () {
      return {
        version: '0.3.3',
          user: {
              isLoggedIn: false,
              nom: '',
              prenom: '',
              login: '',
              email: '',
              id: 0,
              grade: 0
          },
          patient: [],
          ortho: [],
          mots: [],
          motsVersion: 0
      }
    },
    mutations: {
      login (state, payload) {
            state.user.isLoggedIn = true
          state.user.id = payload.id;
          state.user.nom = payload.nom;
          state.user.prenom = payload.prenom;
          state.user.login = payload.login;
          state.user.email = payload.email;
          state.user.grade = payload.grade
      },
      logout (state) {
          state.user = {
                isLoggedIn: false,
                nom: '',
                prenom: '',
                login: '',
                email: '',
                id: 0,
                grade: 0
            }
          state.ortho = []
          state.patient = []
      },
      importPatient (state, payload) {
          state.patient = payload.patient
      },
      addNewPatient (state, payload) {
          state.patient.push(payload.patient)
      },
      removePatient (state, payload) {
          for (let i = 0; i < state.patient.length; i++) {
              if (state.patient[i].id == payload.id) {
                  state.patient.splice(i, 1)
                  break;
              }
          }
      },
      importOrtho (state, payload) {
            state.ortho = payload.ortho
        },
        addNewOrtho (state, payload) {
            state.ortho.push(payload.ortho)
        },
        promoteOrtho (state, payload) {
            for (let i = 0; i < state.ortho.length; i++) {
                if (state.ortho[i].id == payload.id) {
                    state.ortho[i].grade = payload.grade
                }
            }
        },
        removeOrtho (state, payload) {
            for (let i = 0; i < state.ortho.length; i++) {
                if (state.ortho[i].id == payload.id) {
                    state.ortho.splice(i, 1)
                    break;
                }
            }
        },
        importMots (state, payload) {
            state.mots = payload.mots
            state.motsVersion = payload.bdd_version
        },
        addNewMot (state, payload) {
            state.mots.push(payload.mot)
            state.motsVersion += 1
        },
        removeMot (state, payload) {
            for (let i = 0; i < state.mots.length; i++) {
                if (state.mots[i].id == payload.id) {
                    state.mots.splice(i, 1)
                    break;
                }
            }
            state.motsVersion += 1
        }
    },
    plugins: [createPersistedState()],
  })

const app = createApp(App)
app.use(store)
app.use(PrimeVue)
app.directive('tooltip', Tooltip)
app.use(router).use(router).mount('#app')
