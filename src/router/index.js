import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AccountView from '../views/AccountView.vue'
import PatientReportView from '../views/PatientReportView.vue'
import WordListView from '../views/WordListView.vue'
import PanelView from '../views/PanelView.vue'
import OrthoView from "../views/OrthoView.vue"

import json from "../views/json.vue"

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
  },
  {
    path: '/panel',
    name: 'panel',
    component: PanelView
  },
  {
    path: '/account',
    name: 'account',
    component: AccountView
  },
  {
    path: '/json',
    name: 'json',
    component: json
  },
  {
    path: '/patient/:id',
    component: PatientReportView,
  },
  {
    path: '/patient/:id/wordlist/:word',
    name: 'ancientWordlist',
    component: WordListView,
  },
  {
    path: '/patient/:id/wordlist/:word/collab',
    name: 'ancientWordlistcollab',
    component: WordListView,
  },
  {
    path: '/dashboard',
    component: HomeView
  },
  {
    path: '/ortho',
    component: OrthoView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
